import express from 'express'
import { githubLogin, getUser } from './github.js'
import dotenv from 'dotenv'

dotenv.config()

const app = express()

app.get('/', (req, res) => {
  res.redirect(
    `https://github.com/login/oauth/authorize?client_id=${process.env.client_id}`
  )
})
app.get('/github/login', githubLogin)
app.get('/success', getUser)

const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
  console.log('Server started at ' + PORT)
})
