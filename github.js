import axios from 'axios'

var accessToken
const githubLogin = (req, res) => {
  const { code } = req.query
  console.log(code)
  if (!code) {
    return res.send({
      success: false,
      message: 'Error: no code',
    })
  }

  axios({
    method: 'post',
    url: `https://github.com/login/oauth/access_token?client_id=${process.env.client_id}&client_secret=${process.env.client_secrets}&code=${code}`,
    headers: {
      accept: 'application/json',
    },
  }).then((response) => {
    accessToken = response.data.access_token
    //console.log(accessToken)
    res.redirect('/success')
  })
}

const getUser = (req, res, next) => {
  console.log(accessToken)
  axios({
    method: 'get',
    url: `https://api.github.com/user`,
    headers: {
      Authorization: 'token ' + accessToken,
    },
  })
    .then((response) => {
      res.json({ userData: response.data })
    })
    .catch((err) => {
      console.log(err.message)
    })
}

export { githubLogin, getUser }
